Agenda :

1. Docker - I
    1. Docker Hub 
    2. Docker Engine 
    3. Docker Images 
    4. Docker Containers 
    5. Docker Images - Jenkins 
    6. Docker Images - Tomcat 

2. Docker - II
    2.1 Dockerfile 
    2.2 Docker Volumnes 
    2.3 Docker Storage 
    2.4 Microservices 
        2.4.1 Monolithic Applications 

    2.5 Docker Compose 
    2.6 YAML
    2.7 Docker Swarm 
    2.8 Docker Networks 


Dockerfile :

root@jenkins:~/docker-examples# ls -lrt

-rw-r--r-- 1 root root 205 Dec  8 15:54 Dockerfile
-rw-r--r-- 1 root root 138 Dec  8 15:57 index.html

root@jenkins:~/docker-examples# docker build . -t cloudbinary/web001

root@jenkins:~/docker-examples# docker run -d --name WebSite -p 80:80 cloudbinary/web001:latest

Docker Storage/Volumnes : 

# docker volume create my-vol 

docker volume create my-vol

ls -lrt /var/lib/docker/volumes/my-vol/

docker images

docker run -d --name web2 -p 80:80 --mount source=my-vol,destination=/app cloudbinary/web001:latest

docker volume ls

docker volume inspect my-vol

docker volume rm my-vol

#### Bind Mounts #### 

Part of Linux Server :

mkdir web

cd web/

vi index.html

<html>
<head>
    <title>Volumes</title>
</head>
<body bgcolor="olive">

    <h1>I am a Volume</h1>

</body>
</html>

docker images 

docker run -d -v /root/web/:/var/www/html -p 81:80 cloudbinary/web001:latest


root@jenkins:~# pwd
/root
root@jenkins:~# tree
.
├── docker-examples
│   ├── Dockerfile
│   └── index.html
└── web
    └── index.html

2 directories, 3 files
root@jenkins:~# 
root@jenkins:~# cat web/index.html 
<html>
<head>
    <title>Volumes</title>
</head>
<body bgcolor="#FFC300">

    <h1>I am a Volume</h1>

</body>
</html>
root@jenkins:~# docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
cloudbinary/web001         latest              06a7eac54af6        37 minutes ago      285MB
cloudbinary/webserver-kk   version-1.0.0       920e0704d91e        About an hour ago   216MB
httpd                      latest              2ae34abc2ed0        9 days ago          165MB
ubuntu                     latest              775349758637        5 weeks ago         64.2MB
root@jenkins:~# docker run -d -v /root/web/:/var/www/html -p 81:80 cloudbinary/web001:latest
e1b473d6d7638a6bfe33447f888e3cc893518e8cf97584770af4544721e7238c
root@jenkins:~# 
root@jenkins:~# docker ps
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                NAMES
e1b473d6d763        cloudbinary/web001:latest   "/bin/sh -c 'apachec…"   5 seconds ago       Up 4 seconds        0.0.0.0:81->80/tcp   keen_aryabhata
dfc0dc00ac31        cloudbinary/web001:latest   "/bin/sh -c 'apachec…"   8 minutes ago       Up 8 minutes        0.0.0.0:80->80/tcp   web2
root@jenkins:~# cd web/
root@jenkins:~/web# vi index.html 
root@jenkins:~/web# 
root@jenkins:~/web# cat index.html 
<html>
<head>
    <title>Volumes</title>
</head>
<body bgcolor="olive">

    <h1>I am a Volume</h1>

</body>
</html>


# Docker-Compose :

  255  docker --version
  256  curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  257  ls -lrt /usr/local/bin/docker-compose 
  258  chmod +x /usr/local/bin/docker-compose
  259  ls -lrt /usr/local/bin/docker-compose 
  260  docker-compose --version
  261  mkdir my_wordpress
  262  cd my_wordpress/
  263  pwd
  264  history

0^j#hYEtS!B!rxom$z


